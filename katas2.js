function add (a,b) {
    return a + b;
}

function multiply (a,b) {
    let contador = 0;
    let resultado = 0;
    while (contador < b) {
        resultado = add(resultado,a);
        contador ++;
    }
    return resultado;       
}

function power (x,n) {
    let contador = 1;
    let resultado = x;
    while (contador < n) {
        resultado = multiply(x,resultado);
        contador ++;
    }
    return resultado;
}

function factorial(x) {
    let contador = 1;
    let resultado = x;
    while (contador < x) {
        resultado = multiply(contador,resultado);
        contador ++;
    }
    return resultado;

}

function fibonacci(x) {
    if (x == 1) {
        return 0;
    }
    if (x == 2) {
        return 1;
    }
    else {
        let a = 0;
        let b = 1;
        for (let i = 2; i < x; i++){
            resultado = add(a,b);
            a = b;
            b = resultado;
        
        }
    
    }
    return resultado;

}

